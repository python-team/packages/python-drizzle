/* -*- mode: c++; c-basic-offset: 2; indent-tabs-mode: nil; -*-
 *  vim:expandtab:shiftwidth=2:tabstop=2:smarttab:
 *
 * drizzle-interface: Interface Wrappers for Drizzle
 * Copyright (c) 2009 Sun Microsystems
 * Copyright (c) 2010 Monty Taylor and Max Goodman
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

%{
  PyObject *convert_field(drizzle_field_t field, size_t size, drizzle_column_st *column)
  {
    if (size == 0)
    {
      Py_RETURN_NONE;
    }
    else
    {
      switch (drizzle_column_type(column))
      {
      case DRIZZLE_COLUMN_TYPE_TINY_BLOB:   
      case DRIZZLE_COLUMN_TYPE_MEDIUM_BLOB:   
      case DRIZZLE_COLUMN_TYPE_LONG_BLOB:
      case DRIZZLE_COLUMN_TYPE_BLOB:
      case DRIZZLE_COLUMN_TYPE_VAR_STRING:
      case DRIZZLE_COLUMN_TYPE_STRING:
        if ((drizzle_column_flags(column) & DRIZZLE_COLUMN_FLAGS_BINARY) == 1)
          break;
      case DRIZZLE_COLUMN_TYPE_VARCHAR:
        /* Textual fields should be in unicode */
        return PyUnicode_DecodeUTF8(field, size, NULL);
      default:
        return SWIG_FromCharPtrAndSize(field, size);
      }
    }
  }

  PyObject *row_buffer_to_tuple(row_buffer *buffer)
  {
    if (buffer->row == NULL)
    {
      return Py_None;
    }
  
    PyObject *row_tuple= PyTuple_New((Py_ssize_t)(buffer->field_count));
    size_t *field_sizes= drizzle_row_field_sizes(buffer->result);
    uint16_t field;
    for (field= 0; field < buffer->field_count; field++)
    {
      /* Create a Python copy of the field and add it to the tuple */
      drizzle_column_st *field_column= drizzle_column_index(buffer->result, field);
      PyObject *field_obj= convert_field(buffer->row[field], field_sizes[field], field_column);
      PyTuple_SetItem(row_tuple, field, field_obj);
    }
    
    return row_tuple;
  }
%}

%typemap(argout) row_buffer *result_buffer
{
  $result= row_buffer_to_tuple($1);
}

%typemap(argout) row_buffer *new_buffer
{
  $result= row_buffer_to_tuple($1);
	
	if ($1->row != NULL)
  {
    /* Free the previously allocated libdrizzle row buffer */
    drizzle_row_free($1->result, $1->row);
  }
}
