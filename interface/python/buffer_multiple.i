/* -*- mode: c++; c-basic-offset: 2; indent-tabs-mode: nil; -*-
 *  vim:expandtab:shiftwidth=2:tabstop=2:smarttab:
 *
 * drizzle-interface: Interface Wrappers for Drizzle
 * Copyright (c) 2009 Sun Microsystems
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

%extend Result {

  /**
   * Read and buffer one row.
   */
  PyObject *buffer_multiple_rows(uint64_t count)
  {
    uint64_t x;
    drizzle_return_t ret;
    row_buffer new_buffer;
    PyObject *result_list, *row_tuple;
    drizzle_con_st *con = drizzle_result_drizzle_con($self);
    
    result_list = PyList_New(count);
    for (x= 0; x < count; x++) {
      new_buffer.row= drizzle_row_buffer($self, &ret);
      new_buffer.field_count= drizzle_result_column_count($self);
      new_buffer.result= $self;
      
      if (check_drizzle_return(ret, con) == false) {
        return NULL;
      }
  
      if (new_buffer.row == NULL) {
        break;
      }
      
      row_tuple= row_buffer_to_tuple(&new_buffer);
      drizzle_row_free($self, new_buffer.row);
      PyList_SetItem(result_list, x, row_tuple);
    }
    
    if (x != count) {
      // Shrink the list to the number of rows we actually got.
      PyList_SetSlice(result_list, x, count, NULL);
    }
    
    return result_list;
  }
}
